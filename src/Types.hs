{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeOperators #-}
module Types where

import           RIO
import           System.Random.Mersenne
import qualified Katip                         as K
import qualified Servant.Server.Internal       as SI

data App = App
  { appLogNamespace :: K.Namespace
  , appLogContexts :: K.LogContexts
  , appLogEnv :: K.LogEnv
  , appMTGen :: MTGen
  }

class HasLogNamespace env where
  logNamespaceL :: Lens' env K.Namespace
class HasLogContext env where
  logContextL :: Lens' env K.LogContexts
class HasLogEnv env where
  logEnvL :: Lens' env K.LogEnv
class HasMTGen env where
  mtGenL :: Lens' env MTGen

instance HasLogNamespace App where
  logNamespaceL =
    lens appLogNamespace $ \app namespace -> app { appLogNamespace = namespace }
instance HasLogContext App where
  logContextL =
    lens appLogContexts $ \app contexts -> app { appLogContexts = contexts }
instance HasLogEnv App where
  logEnvL = lens appLogEnv $ \app env -> app { appLogEnv = env }
instance HasMTGen App where
  mtGenL = lens appMTGen $ \app gen -> app { appMTGen = gen }

newtype ServantApp a = ServantApp { unServantApp :: ReaderT App SI.Handler a } deriving (Functor, Applicative, Monad, MonadIO, MonadReader App)

instance K.Katip ServantApp where
  getLogEnv = view logEnvL
  localLogEnv f = local $ over logEnvL f

instance K.KatipContext ServantApp where
  getKatipContext = view logContextL
  localKatipContext f = local $ over logContextL f
  getKatipNamespace = view logNamespaceL
  localKatipNamespace f = local $ over logNamespaceL f
