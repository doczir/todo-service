{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}


module Run
    ( run
    )
where

import           Import
import           Servant
import           Data.Aeson
import qualified Network.Wai.Handler.Warp      as W
import  Network.Wai
import qualified Servant.Server.Internal       as SI

type Api = ReqID :> ("healthcheck" :> Get '[JSON] HealthcheckResult 
                :<|> "healthcheck" :> "fancier" :> Get '[JSON] HealthcheckResult)

newtype HealthcheckResult = HealthcheckResult { successful :: Bool } deriving (Generic)
instance ToJSON HealthcheckResult
instance FromJSON HealthcheckResult

data ReqID

instance HasServer api context => HasServer (ReqID :> api) context where
    type ServerT (ReqID :> api) m = Text -> ServerT api m

    route Proxy context subserver =
        route (Proxy :: Proxy api) context (SI.passToServer subserver extractRequestId)
        where
            extractRequestId :: Request -> Text
            extractRequestId req = maybe "" decodeUtf8Lenient $ lookup "X-Request-Id" headers
                where
                    headers = requestHeaders req
    hoistServerWithContext _ pc nt s = hoistServerWithContext (Proxy :: Proxy api) pc nt . s

api :: Proxy Api
api = Proxy

requestContext :: KatipContext m => Text -> m a -> m a
requestContext rid = katipAddContext (sl "request_id" rid)

server :: ServerT Api ServantApp
server = \rid -> requestContext rid healthcheckH :<|> requestContext rid healthcheckH
  where
    healthcheckH :: ServantApp HealthcheckResult
    healthcheckH = do
            $(logTM) InfoS "Oh Hi"
            return $! HealthcheckResult True

toHandler :: App -> ServantApp a -> SI.Handler a
toHandler app servantApp = runReaderT (unServantApp servantApp) app

application :: RIO App Application
application = do
    app <- ask
    return $! serve api $ hoistServer api (toHandler app) server

run :: RIO App ()
run = do
    application' <- application
    liftIO $! W.run 8080 application'
