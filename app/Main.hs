{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Main
    ( main
    )
where

import           Import
import           Run
import           System.Random.Mersenne

main :: IO ()
main = do
    mtGen        <- getStdGen
    handleScribe <- mkHandleScribeWithFormatter jsonFormat ColorIfTerminal stdout InfoS V3
    let makeLogEnv = registerScribe "stdout" handleScribe defaultScribeSettings =<< initLogEnv "MyApp" "production"
    bracket makeLogEnv closeScribes $ \logEnv ->
        let app = App { appLogNamespace = mempty
                      , appLogContexts  = mempty
                      , appLogEnv       = logEnv
                      , appMTGen        = mtGen
                      }
        in  runRIO app run
